/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package configmanager;

import utils.commonutils.CoreUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

//import org.apache.log4j.Logger;

/**
 * 
 * @author Koyel Nandi
 *
 */
public class ConfigManagerHelper {

   // private final static Logger logger = Logger.getLogger(ConfigManagerHelper.class);

    /**
     * Validate file paths specified by user.
     * 
     * @param userConfigPath
     *            Optional
     */
    public static void validateFileTypes(String userConfigPath) {
        if (userConfigPath != null && !userConfigPath.toLowerCase().endsWith(".xml")
                && !userConfigPath.toLowerCase().endsWith(".properties")) {
            throw new IllegalArgumentException(
                    "The userConfigPath file should be either xml or " + "properties file: " + userConfigPath);
        }
    }

    /**
     * Add properties from user specified paths.
     * 
     * @param configManager
     *            configuration manager to add properties to
     * @param userConfigPath
     *            Optional
     */
    public static void addPropertiesFromPath(IConfigManager configManager, String userConfigPath) {

        if (userConfigPath != null) {
            if (CoreUtils.isValidPath(userConfigPath)) {
                configManager.addConfig(userConfigPath);
            } else {
                throw new IllegalArgumentException("Invalid userConfigPath passed: " + userConfigPath);
            }
        }
    }

    /**
     * Add properties in properties file identified by path to ConfigManager.
     * 
     * @param configManager
     *            Required
     * @param path
     *            Required
     */
    public static void addConfig(IConfigManager configManager, String path) {
        InputStream is = null;
        Properties properties = new Properties();
        try {
            is = CoreUtils.toInputStream(path);
            String tmpPath = path.toLowerCase();
            if (tmpPath.endsWith(".properties")) {
                properties.load(is);
            } else if (tmpPath.endsWith(".xml")) {
                properties.loadFromXML(is);
            } else if (tmpPath.endsWith(".json")) {
                throw new UnsupportedOperationException("json support is not implemented yet");
            } else {
                throw new UnsupportedOperationException("only .properties, and .xml files are supported.");
            }
            configManager.addConfig(properties);
        } catch (IOException e) {
            //logger.warn("Error reading configuration from: " + path, e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Apply only to Local settings and valid for the following formats:
     *
     * @param configManager
     *            configuration manager to retrieve the property from.
     * @param configKey
     *            partial property name excluding the platform.
     * @return the property value for the current platform.
     */
    public static String getConfigForThisPlatform(IConfigManager configManager, String configKey) {
        String configValue;
        // Possible values: win(or win32, win64), osx, linux(or linux32, linux64)
        String platform = "";
        // 32, 64 or unknown
        String archDataModel = System.getProperty("sun.arch.data.model");
        if (CoreUtils.isWindows()) {
            platform = "win";
            // skip for windows chrome as there is no separate chrome drivers.
            if (!configKey.contains("chrome")) {
                if (!archDataModel.equalsIgnoreCase("unknown")) {
                    platform = platform + archDataModel;
                }
            }
        } else if (CoreUtils.isMac()) {
            platform = "osx";
        } else if (CoreUtils.isLinux()) {
            platform = "linux";
            if (!archDataModel.equalsIgnoreCase("unknown")) {
                platform = platform + archDataModel;
            }
        }
        if (!platform.equals("")) {
            platform = "." + platform;
        }
        configValue = configManager.getConfig(configKey + platform, true);
        return configValue;
    }


}
