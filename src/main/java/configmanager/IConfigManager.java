/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package configmanager;

import java.util.Map;


/**
 * 
 * @author Koyel Nandi
 *
 */
public interface IConfigManager {

    public void addConfig(String path);

    public void addConfig(Map<? extends Object, ? extends Object> prop);

    public void addConfig(String key, String value);

    public boolean hasValidValueFor(String key);

    public String getConfig(String key, String defaultValue);

    public String getConfig(String key);

    public String getConfig(String key, boolean required);

    public int getConfigAsInt(String key);

}