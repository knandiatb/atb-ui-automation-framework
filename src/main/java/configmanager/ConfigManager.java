/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package configmanager;

//import org.apache.log4j.Logger;


import java.util.Map;
import java.util.Properties;

/**
 * ConfigManager implements a set of properties that is shared across all tests.
 * 
 * @author Koyel Nandi
 */
public final class ConfigManager implements IConfigManager {

   // private static final Logger LOGGER = Logger.getLogger(ConfigManager.class);

    /**
     * Creates one global instance of Properties object.
     */
    private static volatile ConfigManager globalConfigManager;
    private static final Properties globalProperties = new Properties();

    /**
     * Load environment variables then system properties. If property names are same, system property will override the
     * environment.
     */
    private ConfigManager() {
    }

    public static ConfigManager getConfigManager() {
        return createConfigManager();
    }

    public static ConfigManager createConfigManager() {
        return createConfigManager(null);
    }

    /**
     * Creates global configuration manager based on user's specified paths.
     * 
     * @return
     */
    public static ConfigManager createConfigManager(String userConfigPath) {
        synchronized (ConfigManager.class) {
            if (globalConfigManager != null && !globalProperties.isEmpty()) {
                return globalConfigManager;
            } else {
                ConfigManagerHelper.validateFileTypes(userConfigPath);
                globalConfigManager = new ConfigManager();
                ConfigManagerHelper.addPropertiesFromPath(globalConfigManager, userConfigPath);
                globalProperties.putAll(System.getenv());
                globalProperties.putAll(System.getProperties());
                return globalConfigManager;
            }
        }
    }

//    public static String[] getSource(){
//        Method[] methods = new ConfigImport() {
//        }.getClass().getDeclaredMethods();
//        Config.Sources sourceFile = methods[0].getAnnotation(Config.Sources.class);
//        return sourceFile.value();
//    }


    @Override
    public void addConfig(String path) {
        ConfigManagerHelper.addConfig(globalConfigManager, path);
    }

    @Override
    public void addConfig(Map<? extends Object, ? extends Object> prop) {
        globalProperties.putAll(prop);
    }

    @Override
    public void addConfig(String key, String value) {
        globalProperties.put(key, value);
    }

    @Override
    public boolean hasValidValueFor(String key) {
        return hasValue(globalProperties.getProperty(key));
    }

    @Override
    public String getConfig(String key, String defaultValue) {
        return globalProperties.getProperty(key, defaultValue);
    }

    @Override
    public String getConfig(String key) {
        return globalProperties.getProperty(key);
    }

    @Override
    public String getConfig(String key, boolean required) {
        if (key == null || !hasValidValueFor(key)) {
            //LOGGER.error("Required Configuration Key Missing: " + key);
            throw new IllegalArgumentException("Requried Configuration Key Missing: " + key);
        }
        return globalProperties.getProperty(key);
    }

    @Override
    public int getConfigAsInt(String key) {
        Object value = globalProperties.get(key);
        return Integer.parseInt(value.toString());
    }


    /**
     * Indicates if the given String contains a value (not null and size &gt; 0)
     *
     * @param pString
     */
    public static boolean hasValue(String pString) {
        return ((pString == null) || (pString.trim().length() == 0)) ? false : true;
    }


    public static void dump(String envelope) {
//        LOGGER.info("+++++++++++++++ " + envelope + "+++++++++++++++");
//        LOGGER.info("GridLevel.Local.ENABLED: " + globalProperties.getProperty("GridLevel.Local.ENABLED"));
//        LOGGER.info("GridLevel.PrivateGrid.ENABLED: " + globalProperties.getProperty("GridLevel.PrivateGrid.ENABLED"));
//        LOGGER.info("GridLevel.CommonGrid.ENABLED: " + globalProperties.getProperty("GridLevel.BenevityGrid.ENABLED"));
//        LOGGER.info("GridLevel.VendorGrid.ENABLED: " + globalProperties.getProperty("GridLevel.CloudGrid.ENABLED"));
//        LOGGER.info("++++++++++++++++++++++++++++++++++++++++++++++++");
    }

}
