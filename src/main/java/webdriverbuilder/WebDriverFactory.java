/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package webdriverbuilder;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.exceptions.UnableToCreateWebDriverException;


/**
 * The factory class used for creating WebDriver.
 *
 * @author Koyel Nandi
 */
public class WebDriverFactory {

    private static OptionsManager optionsManager = new OptionsManager();
    private static ThreadLocal<WebDriver> tlDriver = new ThreadLocal<>();

    public static synchronized void setDriver(String browser) throws UnableToCreateWebDriverException {
        if (browser.equals("firefox")) {
            tlDriver = ThreadLocal.withInitial(() -> new FirefoxDriver(optionsManager.getFirefoxOptions()));

            //For Grid Usage
            /*try {
                tlDriver.set(new RemoteWebDriver(new URL("http://abc.test"), optionsManager.getFirefoxOptions()));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }*/
        } else if (browser.equals("chrome")) {
            tlDriver.set(new ChromeDriver(optionsManager.getChromeOptions()));

             /*//For Grid Usage
            try {
                tlDriver.set(new RemoteWebDriver(new URL("http://abc.com"), optionsManager.getChromeOptions()));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }*/
        }
    }

    public static synchronized WebDriverWait getWait (WebDriver driver) {
        return new WebDriverWait(driver,20);
    }

    public static synchronized WebDriver getDriver() {
        return tlDriver.get();
    }
}