/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */

package utils.commonutils;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * The helper class of all util methods.
 *
 * @author Nick Grover
 * @author Norbert Griess
 */

public class AutomationHelper {
    // Retrieves current date formated by given format string
    public static String getFormatedDate(String format)
    {
        Date date = new Date();
        return new SimpleDateFormat(format).format(date);
    }
    // Retrieves the current date as ISO 8601 string
    public static String getISOdate()
    {
        return getFormatedDate("yyyy-MM-dd");
    }
    // Retrieves the current time as ISO 8601 string
    public static String getISOtime()
    {
        return getFormatedDate("HH:mm:ss");
    }
    public static String convertToISODate(Date date)
    {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }
    // Takes a given string amount, removes alphanumerical and special characters and converts it into a double
    public static double convertStringIntoDouble (String amount)
    {
        String rawAmount = amount.replaceAll("[^0-9.]", "");
        return Double.parseDouble(rawAmount);
    }
    // Converts a double value into a currency string (including adding $ sign and adding ',' thousands separator)
    public static String convertDoubleIntoCurrencyString(double amount)
    {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CANADA);
        return format.format(amount);

    }
    // Converts a given date object into a transaction format date
    public static String convertDateToTransactionFormat(Date date)
    {
        String transactionFormat = "MMMM dd, YYYY";
        return new SimpleDateFormat(transactionFormat).format(date);
    }
    // Takes a transaction date and converts it into a date object
    public static Date convertTransactionDateIntoDateObject(String transactionDate)
    {
        // Happenings and Going On's are occurring in this magical conversion
        Date daDate = null;
        try {
            transactionDate = transactionDate.replace("Today, ", "");
            String td = transactionDate.replace(",", "");
            String[] transactionDateFractions = td.split(" ");
            Calendar cal = Calendar.getInstance();
            Date specialMonthConversionDate = new Date();
            try {
                specialMonthConversionDate = new SimpleDateFormat("MMMM", Locale.CANADA).parse(transactionDateFractions[0]);
            } catch (Exception e) {
                // it appears that the conversion failed.
            }
            cal.setTime(specialMonthConversionDate);
            cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(transactionDateFractions[1]));
            cal.set(Calendar.YEAR, Integer.parseInt(transactionDateFractions[2]));
            daDate = cal.getTime();
        }
        catch (Exception e)
        {
            ///igrnore
        }
        finally {
            return daDate;
        }


    }
    // Converts a given date object into a old imperial date format
    public static String convertDateToImperialFormat(Date date)
    {
        String oldImperial = "MM/dd/yyyy";
        return  new SimpleDateFormat(oldImperial).format(date);
    }
    // Converts a given date object into a old imperial date format
    public static String convertDateToImperialFormatNoNull(Date date)
    {
        String oldImperial = "M/d/yyyy";
        return  new SimpleDateFormat(oldImperial).format(date);
    }
    // Converts given ISO string into a Date object
    public static Date convertISOdateIntoDateObject(String isoDate)
    {
        String[] dateFractions= isoDate.split("-");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.parseInt(dateFractions[0]));
        int getMonthNumber = Integer.parseInt(dateFractions[1]);
        cal.set(Calendar.MONTH, getMonthNumber-1);
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateFractions[2]));
        return cal.getTime();
    }
    // Retrieves the current date and dates it back into the past by indicated number of days.
    public static Date getCurrentDateMinusDays(int days)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, days*(-1));
        return calendar.getTime();
    }
    public static boolean areDatesEqual(Date date1, Date date2)
    {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);


    }


}
