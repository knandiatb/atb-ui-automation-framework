/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */

package utils.commonutils;


import one.util.streamex.StreamEx;
import org.apache.commons.lang.StringUtils;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * The root class of all Container implementation. This class should be used as means for delegating any iteration
 * needed in page object when working with grids or tables.
 *
 * @author Koyel Nandi
 *
 */
public abstract class Container extends AbstractScreen {

    protected List<WebElement> parentElement;
    private int index;


    /**
     * Construct a basic container instance. This constructor initializes list of WebElement we define as parentElement.
     * All child elements that needs to be inspected by PageObject will iterate this Parent WebElement List.
     *
     * @param parentElement
     *            the List of WebElements we define as parent element
     */
    public Container(List<WebElement> parentElement) {
        this(parentElement, null);
    }

    /**
     * Construct a container instance. This constructor initializes list of WebElement we define as parentElement along
     * with the driver instance. This constructor should be used for iterating a list of child elements across different
     * pages. It uses the default nextButtonBy locator.
     *
     * @param parentElement
     *            the List of WebElements we define as parent element
     * @param driver
     *            the driver instance currently on a given page
     */
    public Container(List<WebElement> parentElement, WebDriver driver) {
        this(parentElement, driver, null);
    }

    /**
     * Construct a container instance. This constructor initializes list of WebElement we define as parentElement, the
     * driver instance as well as custom nextButtonBy locator to override the default. This constructor should be used
     * for iterating a list of child elements across different pages where a nextButtonBy locator is different to the
     * default one provided by this class.
     *
     * @param parentElement
     *            the List of WebElements we define as parent element
     * @param driver
     *            the driver instance currently on a given page
     * @param nextButtonBy
     *            the next button by locator present on the page to override the default provided by this class
     */
    public Container(List<WebElement> parentElement, WebDriver driver, By nextButtonBy) {
        super(driver);
        this.parentElement = parentElement;
    }

    /**
     * Convenience method to set the index in event a get needs to be performed on parentElement
     *
     * @param index
     *            the new index
     */
    protected void setIndex(int index) {
        this.index = index;
    }

    /**
     * Convenience method to get the size of parentElement List
     *
     * @return int the size of parent element list
     */
    protected int getSize() {
        return parentElement.size();
    }

    /**
     * A service method that provides WebElement based on a By locator and text that matches the WebElement. It performs
     * an absolute match with the text. This method can throw NoSuchElement Exception that needs to be handled
     * appropriately by the caller.
     *
     * @param by
     *            the by locator that needs to be inspected
     * @param text
     *            the text we use to perform exact match with element's text
     * @return the element the WebElement that matches the criteria
     */
    protected WebElement getElementByText(By by, String text) {
        Predicate<WebElement> elementWithMatchingText = we -> we.findElement(by).getText().equals(text);
        return getWebElementBasedOn(elementWithMatchingText);
    }

    /**
     * A service method that provides WebElement based on a By locator and text that matches the WebElement. It performs
     * a partial match with the text. This method can throw NoSuchElement Exception that needs to be handled
     * appropriately by the caller.
     *
     * @param by
     *            the by locator that needs to be inspected
     * @param text
     *            the text we use to perform partial match with element's text
     * @return the element the WebElement that matches the criteria
     */
    protected WebElement getElementByPartialText(By by, String text) {
        Predicate<WebElement> elementWithMatchingPartialText = we -> we.findElement(by).getText().contains(text);
        return getWebElementBasedOn(elementWithMatchingPartialText);
    }

    /**
     * A service method that provides WebElement based on a By locator and text that matches the WebElement's value
     * attribute. It performs an absolute match with the text. This method can throw NoSuchElement exception that needs
     * to be handled appropriately by the caller.
     *
     * @param by
     *            the by locator that needs to be inspected
     * @param text
     *            the text we use to perform exact match with element's value attribute text
     * @return the element the WebElement that matches the criteria
     */
    protected WebElement getElementByValue(By by, String text) {
        Predicate<WebElement> elementValueWithMatchingText = we -> we.findElement(by).getAttribute("value")
                .equals(text);
        return getWebElementBasedOn(elementValueWithMatchingText);
    }

    /**
     * A service method that provides WebElement based on a By locator and text that matches the WebElement's value
     * attribute. It performs a partial match with the text. This method can throw NoSuchElement exception that needs to
     * be handled appropriately by the caller.
     *
     * @param by
     *            the by locator that needs to be inspected
     * @param text
     *            the text we use to perform partial match with element's value attribute text
     * @return the element the WebElement that matches the criteria
     */
    protected WebElement getElementByPartialValue(By by, String text) {
        Predicate<WebElement> elementValueWithMatchingPartialText = we -> we.findElement(by).getAttribute("value")
                .contains(text);
        return getWebElementBasedOn(elementValueWithMatchingPartialText);
    }

    /**
     * A service method that provides an index based on a By locator and text that matches the WebElement. It performs
     * an exact match with the text. This method will return -1 if search criteria is not met. This is for the
     * convenience of the caller to throw or handle appropriately.
     *
     * @param by
     *            the by locator that needs to be inspected
     * @param text
     *            the text we use to perform an exact match with element text
     * @return int index of the WebElement that matches the criteria
     */
    protected int getElementIndexByText(By by, String text) {
        Predicate<WebElement> elementWithMatchingText = we -> we.findElement(by).getText().equals(text);
        return getIndexForElementBasedOn(elementWithMatchingText);
    }

    /**
     * A service method that provides an index based on a By locator and text that matches the WebElement. It performs a
     * partial match with the text. This method will return -1 if search criteria is not met. This is for the
     * convenience of the caller to throw or handle appropriately.
     *
     * @param by
     *            the by locator that needs to be inspected
     * @param text
     *            the text we use to perform partial match with element text
     * @return int index of the WebElement that matches the criteria
     */
    protected int getElementIndexByPartialText(By by, String text) {
        Predicate<WebElement> elementWithMatchingPartialText = we -> we.findElement(by).getText().contains(text);
        return getIndexForElementBasedOn(elementWithMatchingPartialText);
    }

    /**
     * A service method that provides an index based on a By locator and text that matches the WebElement's value
     * attribute. It performs an exact match with the text. This method will return -1 if search criteria is not met.
     * This is for the convenience of the caller to throw or handle appropriately.
     *
     * @param by
     *            the by locator that needs to be inspected
     * @param text
     *            the text we use to perform exact match with element's value attribute text
     * @return int index of the WebElement that matches the criteria
     */
    protected int getElementIndexByValue(By by, String text) {
        Predicate<WebElement> elementValueWithMatchingText = we -> we.findElement(by).getAttribute("value")
                .equals(text);
        return getIndexForElementBasedOn(elementValueWithMatchingText);
    }

    /**
     * A service method that provides an index based on a By locator and text that matches the WebElement's value
     * attribute. It performs a partial match with the text. This method will return -1 if search criteria is not met.
     * This is for the convenience of the caller to throw or handle appropriately.
     *
     * @param by
     *            the by locator that needs to be inspected
     * @param text
     *            the text we use to perform a partial match with element's value attribute text
     * @return int index of the WebElement that matches the criteria
     */
    protected int getElementIndexByPartialValue(By by, String text) {
        Predicate<WebElement> elementValueWithMatchingPartialText = we -> we.findElement(by).getAttribute("value")
                .contains(text);
        return getIndexForElementBasedOn(elementValueWithMatchingPartialText);
    }

    /**
     * A service method that provides an index based on a By locator and text that matches the WebElement's value
     * attribute. It performs a case insensitive exact match with the text. This method will return -1 if search
     * criteria is not met. This is for the convenience of the caller to throw or handle appropriately.
     *
     * @param by
     *            the by locator that needs to be inspected
     * @param text
     *            the text we use to perform a case insensitive exact match with element's text
     * @return int index of the WebElement that matches the criteria
     */
    protected int getElementIndexByTextIgnoringCase(By by, String text) {
        Predicate<WebElement> elementWithMatchingTextIgnoringCase = we -> StringUtils
                .equalsIgnoreCase(we.findElement(by).getText(), text);
        return getIndexForElementBasedOn(elementWithMatchingTextIgnoringCase);
    }

    /**
     * A service method that provides an index based on a By locator and text that matches the WebElement's value
     * attribute. It performs a case insensitive partial match with the text. This method will return -1 if search
     * criteria is not met. This is for the convenience of the caller to throw or handle appropriately.
     *
     * @param by
     *            the by locator that needs to be inspected
     * @param text
     *            the text we use to perform a case insensitive partial match with element's text
     * @return int index of the WebElement that matches the criteria
     */
    protected int getElementIndexByPartialTextIgnoringCase(By by, String text) {
        Predicate<WebElement> elementWithMatchingPartialTextIgnoringCase = we -> StringUtils
                .containsIgnoreCase(we.findElement(by).getText(), text);
        return getIndexForElementBasedOn(elementWithMatchingPartialTextIgnoringCase);
    }

    /**
     * An internal method that uses a search criteria to perform a check. It is a short circuit method that returns the
     * first matching index for the WebElement.
     *
     * @param criteria
     *            criteria of Java's Predicate Class. This criteria will be used to perform a test and find the first
     *            successful match.
     * @return int the index for web element that matches the criteria
     */
    private int getIndexForElementBasedOn(Predicate<WebElement> criteria) {
        return (int) StreamEx.of(parentElement)
                .indexOf(criteria)
                .orElse(-1);
    }

    /**
     * A service method that provides an index based on a By locator and text that contains in the WebElement's
     * <attribute>'s value. It performs a partial match with the text. This method will return -1 if search criteria is
     * not met. This is for the convenience of the caller to throw or handle appropriately.
     *
     * @param by
     *            the by locator that needs to be inspected
     * @param attribute
     *            the attribute name of the WebElement we use to get the value
     * @param text
     *            the text we use to perform a partial match with element's <attribute> attribute text
     * @return int index of the WebElement that matches the criteria
     */
    protected int getElementIndexByPartialAttributeValue(By by, String attribute, String text) {
        int i = 0;
        for (WebElement element : parentElement) {
            if (element.findElement(by).getAttribute(attribute).contains(text)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    /**
     * An internal method that uses a search criteria to perform a check. It is a short circuit method that returns the
     * first matching WebElement.
     *
     * @param criteria
     *            criteria of Java's Predicate Class. This criteria will be used to perform a test and find the first
     *            successful match.
     * @return WebElement the web element that matches the criteria
     */
    private WebElement getWebElementBasedOn(Predicate<WebElement> criteria) {
        return parentElement.stream().filter(we -> criteria.test(we)).findFirst().get();
    }

    /**
     * A service method that provides an index based on a list of Pair of type By locator and text. It expects a couple
     * of pairs as list and performs a check on the text of webelement. If a match is found, it returns the index else
     * returns a negative 1.
     *
     * @param pairList List of Pairs that holds the By locator and text.
     * @return int index of the WebElement that matches
     */
    protected int getElementIndexByText(List<org.apache.commons.lang3.tuple.Pair<By, String>> pairList) {
        return (int) StreamEx.of(parentElement).indexOf(
                e -> StreamEx.of(pairList).allMatch(p -> e.findElement(p.getLeft()).getText().equals(p.getRight())))
                .orElse(-1);
    }

    /**
     * Convenience method that provides a WebElement based on an index and By locator strategy. It uses the parent
     * element list to do a look up.
     *
     * @param index
     *            the index that needs to be fetched
     * @param by
     *            the by locator strategy that is needed to lookup WebElement
     * @return the element the WebElement that is present at the given index looked up via provided by strategy.
     */
    protected WebElement getElement(int index, By by) {
        return parentElement.get(index).findElement(by);
    }

    /**
     * Convenience method that provides a List of WebElement based on an index and By locator strategy. It uses the
     * parent element list to do a look up.
     *
     * @param index
     *            the index that needs to be fetched
     * @param by
     *            the by locator strategy that is needed to lookup WebElement
     * @return the element list the List of WebElement that is present at the given index looked up via provided by
     *         strategy.
     */
    protected List<WebElement> getElements(int index, By by) {
        return parentElement.get(index).findElements(by);
    }

    /**
     * Convenience method that provides a WebElement based on a By locator strategy. It uses the parent element list to
     * do a look up.
     *
     * @param by
     *            the by locator strategy that is needed to lookup WebElement
     * @return the element the WebElement that is present at the given index looked up via provided by strategy.
     */
    protected WebElement getElement(By by) {
        return parentElement.get(index).findElement(by);
    }

    /**
     * Convenience method that checks if a child element is present at a given index using a by locator strategy.
     *
     * @param index
     *            the index that needs to be fetched
     * @param by
     *            the by locator strategy that is needed to lookup WebElement
     * @return boolean true if the child element is present else returns false
     */
    protected Boolean isChildElementPresent(int index, By by) {
        return parentElement.get(index).findElements(by).size() > 0;
    }

    /**
     * Convenience method that checks if is next button is displayed on the page. This is used to perform paginated
     * searches.
     *
     * @return boolean true, if next button is displayed
     */
    protected boolean isNextButtonDisplayed() {
        //return isElementPresent(nextButtonBy);
        return false;
    }


    /**
     * Convenience method that provides List of element by text. This method will use By locator strategy to find
     * elements and then perform getText() operation on it.
     *
     * @param by
     *            the by locator strategy to search for a WebElement
     * @return List<String> the list of element's tex
     */
    protected List<String> getTextList(By by) {
        return parentElement.stream().map(ele -> ele.findElement(by).getText()).collect(Collectors.toList());
    }

}