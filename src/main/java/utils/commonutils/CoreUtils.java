/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package utils.commonutils;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;





/**
 * Common Utilities
 * 
 * @author Koyel Nandi
 */

public class CoreUtils {

    public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
    public static final String ENCODING = "UTF-8";
    public static final String EOL = System.getProperty("line.separator");

    public static String getOSName() {
        String osName = System.getProperty("os.name");
        return osName;
    }

    public static boolean isWindows() {
        return getOSName().toLowerCase().startsWith("win");
    }

    public static boolean isMac() {
        return CoreUtils.getOSName().toLowerCase().contains("mac");
    }

    public static boolean isLinux() {
        return CoreUtils.getOSName().toLowerCase().indexOf("nux") > 0;
    }

    public static void validateArgument(String arg) {
        if (hasNotValue(arg)) {
            throw new IllegalArgumentException("Invalid argument: " + arg);
        }
    }

    /**
     * Indicates if the given String contains a value (not null and size &gt; 0)
     *
     * @param pString
     */
    public static boolean hasValue(String pString) {
        return ((pString == null) || (pString.trim().length() == 0)) ? false : true;
    }

    /**
     * Indicates if the given String contains no value (null or size &lt;= 0)
     *
     * @param pString
     */
    public static boolean hasNotValue(String pString) {
        return !hasValue(pString);
    }

    /**
     * Indicates if the given String Array contains a value (not null and size &lt; 0)
     *
     * @param pStringArray
     */
    public static boolean hasValue(String[] pStringArray) {
        return ((pStringArray == null) || (pStringArray.length == 0)) ? false : true;
    }

    /**
     * Indicates if the given String Array contains no value (null or size &lt;= 0)
     *
     * @param pStringArray
     */
    public static boolean hasNotValue(String[] pStringArray) {
        return !hasValue(pStringArray);
    }

    /**
     * Returns the current time in a String
     */
    public static String now() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        return sdf.format(cal.getTime());
    }

    /**
     * Returns the current millisecond time in a long
     */
    public static long getCurrentMillisecondTime() {
        Date date = new Date();
        return date.getTime();
    }

    /**
     * Returns a comma separated list of elements included in the given array
     *
     * @param pStrings
     *            Array of Strings
     */
    public static String displayArray(String[] pStrings) {
        StringBuilder result = new StringBuilder();
        result.append("{");
        if (pStrings != null && pStrings.length > 0) {
            for (int i = 0; i < pStrings.length; i++) {
                String item = pStrings[i];
                result.append(item);
                if (i < (pStrings.length - 1)) {
                    result.append(",");
                }
            }
        }
        result.append("}");
        return result.toString();
    }

    /**
     * Returns a comma separated list of elements included in the given array
     *
     * @param pStrings
     *            Array of Strings
     */
    public static String displayArray(String[][] pStrings) {
        StringBuilder result = new StringBuilder();
        result.append("{");
        if (pStrings != null && pStrings.length > 0) {
            for (int i = 0; i < pStrings.length; i++) {
                String[] item = pStrings[i];
                result.append(displayArray(item));
                if (i < (pStrings.length - 1)) {
                    result.append(",");
                }
            }
        }
        result.append("}");
        return result.toString();
    }

    /**
     * Returns only the class name without the package path
     *
     * @param pClass
     *            String with the class name (with or without package path)
     */
    public static String getClassName(String pClass) {
        if (pClass == null) {
            return null;
        }
        String result = pClass;
        int firstChar = pClass.lastIndexOf('.') + 1;
        if (firstChar > 0) {
            result = pClass.substring(firstChar);
        }
        return result;
    }

    /**
     * Sleeps a certain number of ms...
     *
     * @param pTimeInMs
     */
    public static void sleep(int pTimeInMs) {
        try {
            Thread.sleep(pTimeInMs);
        } catch (InterruptedException e) {
            // do nothing...
        }
    }

    /**
     * If the given String is null, the empty String is returned, otherwise the original String is then returned
     *
     * @param pString
     */
    public static Object noNull(String pString) {
        if (pString == null) {
            return "";
        } else {
            return pString;
        }
    }

    /**
     * Checks if the given string contains a valid integer
     *
     * @param pNumber
     */
    public static boolean isInteger(String pNumber) {
        if (!hasValue(pNumber)) {
            return false;
        }
        try {
            Integer.parseInt(pNumber);
            return true;
        } catch (Throwable e) {
            return false;
        }
    }

    /**
     * Check if the a String is contained in an String array...
     *
     * @param pName
     *            String
     * @param pList
     *            String array
     * @param pOptimistic
     *            if true, the returned value will be TRUE if the input array in empty or null
     */
    public static boolean containsString(String pName, String[] pList, boolean pOptimistic) {
        if ((pName == null) || (pList == null)) {
            if (pOptimistic == true) {
                return true;
            } else {
                return false;
            }
        }
        for (int i = 0; i < pList.length; i++) {
            String item = pList[i];
            if (item.contains(pName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Search occurrence of the given pattern in the given source.
     *
     * @param src
     * @param pattern
     *            <code>null</code> is never returned
     */
    public static List<String> findStringsByPattern(String src, String pattern) {
        List<String> result = new ArrayList<String>();
        if (!CoreUtils.hasValue(src)) {
            return result;
        }
        String source = src.replace(CoreUtils.EOL, " ");
        Pattern elementPattern = Pattern.compile(pattern);
        Matcher elementMatcher = elementPattern.matcher(source);
        while (elementMatcher.find()) {
            String item = elementMatcher.group().trim();
            result.add(item);
        }
        return result;
    }

    /**
     * Parse an integer in String format
     *
     * @param value
     */
    public static int parseInteger(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            System.out.println("Integer cannot be parsed: " + value);
            throw e;
        }
    }

    public static boolean parseBoolean(String value) {
        if (!hasValue(value)) {
            return false;
        }
        return (value.trim().equalsIgnoreCase("true")) ? true : false;
    }

    public static String getCurrentTestClassName() {
        String cname = null;
        for (StackTraceElement one : Thread.currentThread().getStackTrace()) {
            cname = one.getClassName();

            if (cname != null && cname.contains("Test")) {
                break;
            }
        }
        return cname;
    }

    public static String getUniqueIdentifier() {
        return Long.toHexString(System.nanoTime());
    }

    public static String extractValueFromNV(String src, String key, String delimeter) {
        String ret = "";
        String tmp = "";
        int i = src.indexOf(key);
        if (i != -1) {
            tmp = src.substring(i + key.length());
            String[] tmps = tmp.split(delimeter);
            if (tmps.length > 0) {
                tmp = tmps[0];
                i = tmp.indexOf('=');
                if (i != -1) {
                    ret = tmp.substring(i + 1).trim();
                }
            }
        }
        return ret;
    }

    public static String mapToString(Map<?, ?> map) {
        String ret = "{\n";
        for (Object key : map.keySet()) {
            ret += (" " + key + " = " + map.get(key) + "\n");
        }
        return ret + "}\n";
    }






    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // File Utilities //////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Just load and return text from the given path that could be either a file path or a resource path. If a resource
     * path/name is given, the class loader of ICUtils will try to search and load. When loading the text, this function
     * will read it with UTF8 encoding.
     *
     * @param path
     *            A path string for either a file or resource.
     * @return Content as string for the given file or resource.
     */
    public static String loadTextFrom(String path) {
        validateArgument(path);
        BufferedReader reader = null;
        StringBuilder buffer = new StringBuilder();
        String content = null;
        try {
            InputStream is = CoreUtils.class.getResourceAsStream(path);
            if (is == null) {
                is = new FileInputStream(path);
            }
            reader = new BufferedReader(new InputStreamReader(is, ENCODING));
            String str;
            while ((str = reader.readLine()) != null) {
                buffer.append(str);
            }
            content = buffer.toString();
            reader.close();
        } catch (IOException e) {
            System.out.println("Error reading file from: " + path);
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ex) {
                }
            }
        }
        return content;
    }

    public static boolean resourceExists(String path) {
        if (CoreUtils.hasNotValue(path))
            return false;
        boolean exists = false;
        InputStream is;
        try {
            is = CoreUtils.class.getResourceAsStream(path);
            if (is == null) {
                is = new FileInputStream(path);
            }
            is.close();
            exists = true;
        } catch (IOException e) {
        }
        return exists;
    }


    /**
     * Load a property file to java Properties object.
     * 
     * @param path
     *            a path of either file or resource path.
     * @return
     */
    public static Properties loadProperties(String path) {
        InputStream is = null;
        Properties props = new Properties();
        if (CoreUtils.resourceExists(path)) {
            try {
                is = CoreUtils.toInputStream(path);
                props.load(is);
            } catch (IOException ex) {
                System.out.println("Couldn't load " + path);
                return null; // propagation is better
            }
        } else {
            System.out.println("Couldn't find: " + path);
            return null;
        }
        return props;
    }

    /**
     * Creates a file that contains the given binary data
     *
     * @param pFile
     *            File where the data will be located
     * @param pData
     *            Byte array with the data
     */
    public static void writeFile(File pFile, byte[] pData) {
        if (pData == null || pData.length < 1) {
            System.out.println("Data is empty, file " + pFile + " will not be generated.");
        } else {
            try {
                if (pFile.exists()) {
                    System.out.println("File " + pFile + " already exists. It will be overwritten !");
                    pFile.delete();
                }
                File path = new File(pFile.getParent());
                path.mkdirs();
                path = null;

                pFile.createNewFile();
                BufferedOutputStream bw = new BufferedOutputStream(new FileOutputStream(pFile));
                // it is binary data, encoding should not be set!!
                // OutputStreamWriter bw = new OutputStreamWriter(new FileOutputStream(pFile),Utils.ENCODING);
                bw.write(pData);
                bw.flush();
                bw.close();
            } catch (Throwable e) {
                System.out.println("Error trying to create file " + pFile + " with data: " + CoreUtils.EOL + pData
                        + CoreUtils.EOL + e.getMessage());
            }
        }
    }

    /**
     * Write inputString to file with fileName
     *
     * @param fileName
     * @param inputString
     * @throws IOException
     */
    public static void writeStringToFile(String fileName, String inputString) throws IOException {
        // write String to file
        BufferedWriter outWriter = new BufferedWriter(new FileWriter(fileName));
        outWriter.write(inputString);
        outWriter.close();
    }

    /**
     * Returns an input stream out of the given file or resource path.
     * 
     * @param path
     *            A path string for either a file or resource.
     * @return
     * @throws IOException
     */
    public static InputStream toInputStream(String path) throws IOException {
        if (hasNotValue(path)) {
            throw new IOException("Empty value passed.");
        }
        InputStream is = CoreUtils.class.getResourceAsStream(path);
        if (is == null) {
            File file = new File(path);
            if (!file.isDirectory()) {
                if (file.exists()) {
                    is = new FileInputStream(file);
                } else {
                    throw new IOException("File not exist: " + path);
                }
            } else {
                throw new IOException("Unexpected dir path passed (a file path should be passed): " + path);
            }
        }
        return is;
    }

    /**
     * Returns true if the path is valid string and its file is existing.
     * 
     * @param path
     *            A file or resource path to be tested
     * @return
     */
    public static boolean isValidPath(String path) {
        if (CoreUtils.hasValue(path)) {
            // check if this can be found as a resource.
            if (CoreUtils.class.getResourceAsStream(path) != null) {
                return true;
            } else {
                // check if this can be found in a file system
                return new File(path).exists();
            }
        }
        return false;
    }
}
