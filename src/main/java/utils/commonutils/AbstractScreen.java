/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */

package utils.commonutils;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;
import org.testng.Assert;
import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.fail;

/**
 * The root class of all Page Object implementation. This class can become a basic, frame, context sensitive, container,
 * and component page object type, and any combination thereof.
 *
 * @author Nick Grover
 * @author Norbert Griess
 */

public abstract class AbstractScreen extends AutomationHelper {

    //region Attributes
    public WebDriver driver;
    public final static int TIMEOUT = 60; // in seconds
    private final static byte longerWait = 5; // in seconds
    private final static int millisecondsToSleep = 1000; //in milliseconds

    //endregion

        public AbstractScreen(WebDriver driver) {
        this.driver = driver;
    }
    // This method accepts the currently showing popu-up alert
    public void acceptAlert()
    {
        Alert alert = driver.switchTo().alert();
        log("ACCEPTING ALERT: "+alert.getText());
        alert.accept();
    }
    public void authenticateAltert(String userName, String password)
    {
        Alert alert = driver.switchTo().alert();
        log("Authenticating ALERT: "+alert.getText());
        alert.sendKeys(userName);
        alert.sendKeys(Keys.TAB.toString());
        alert.sendKeys(password);
        alert.sendKeys(Keys.RETURN.toString());
//
    }
    // Clicks on the element associated to the given locator expression
    public void click(String elementLocator)
    {
        WebElement element = getElement(elementLocator);
        click(element);
    }
    public void click(WebElement element)
    {
        waitForObjectVisible(element);
        logInteraction(element, "CLICK");
        element.click();
    }
    public void clickButton(String elementLocator)
    {
        log("Clicking on button:'"+elementLocator+"'...");
        WebElement element = getElement(elementLocator);
        waitForObjectEnabled(element);
        element.sendKeys(Keys.RETURN);

    }
    public void typeTab(String elementLocator)
    {
        WebElement element = getElement(elementLocator);
        typeTab(element);

    }
    public void typeTab(WebElement element)
    {
        log("Typing 'TAB' key...");
        element.sendKeys(Keys.TAB);
    }
    // Clicks on a label element that is associated with a containing text
    public void clickLabel(String text)
    {
        if (text!=null) {
            clickElementContainingText("label", text);
        }
    }
    public void clickJavaScript(String text)
    {
        log("Running a click based on javaScript execution...");
        log(text);
        WebElement element = getElement(text);
        clickJavaScript(element);
    }
    public void clickJavaScript(WebElement element)
    {
        waitForObjectEnabled(element);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", element);
    }
    // Clicks on a list element that is associated with a containing text
    public void clickListItem(String text)
    {
        clickElementContainingText("li", text);
    }
    // Clicks on a given element type that is associated with a containing text
    private void clickElementContainingText(String elementType, String text)
    {
        click("//"+elementType+"[contains(text(),'"+text+"')]");
    }
    // Clicks on the element associated to the given locator expression
    public  void fireJavaScript(String elementLocator, String function)
    {
        log("Executing a java script click on..."+elementLocator);
        WebElement element = getElement(elementLocator);
        waitForObjectVisibleEnabled(element);
        logInteraction(element, "CLICK");
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript(function+";");
    }
    // Clicks on a Link associated to the given locator expression
    public void clickLink(String linkName)
    {
        WebElement element = getElementByLink(linkName);
        waitForObjectVisible(element);
        logInteraction(element, "CLICK");
        element.click();
    }
    // Types into the element associated to the given locator expression
    public void type(String elementLocator, String input)
    {
        if (input == null || input==""){return;}
        WebElement element = getElement(elementLocator);
        waitForObjectVisible(element);
        type(element, input);
    }
    public void type(WebElement element, String input)
    {
        logInteraction(element, "TYPE text '"+input+"'");
        element.sendKeys(input);
    }
    // Types a key into the element associated to the given locator expression
    public void type(String elementLocator, Keys key)
    {
        if (key == null){return;}
        WebElement element = getElement(elementLocator);
        waitForObjectVisible(element);
        logInteraction(element, "TYPE key '"+key.toString()+"'");
        element.click();
        type(key);

    }
    // Types a key independed from any selected element.
    public void type(Keys key)
    {
        log("Typing: "+key);
        Actions builder = new Actions(driver);
        builder.sendKeys(key).perform();
    }
    // Types a string independed from any selected element.
    public void type(String text)
    {
        log("Typing: "+text);
        if(text==null || text.isEmpty())
        {
            return;
        }
        Actions builder = new Actions(driver);
        builder.sendKeys(text).perform();
    }
    // Clears the current control before it types in given input string
    public void clearType(String elementLocator, String input)
    {
        log("Clearing textfield and typing '"+input+"'...");
        WebElement element = getElement(elementLocator);
        waitForObjectVisible(element);
        element.sendKeys(Keys.CONTROL,"a");
        sleep();
        element.sendKeys(Keys.DELETE);
        element.sendKeys(input);

    }
    // Clears the current control before it types in given input string
    public void clearType()
    {
        log("Clearing selected field...");
        Actions builder = new Actions(driver);
        builder.sendKeys(Keys.CONTROL,"a").perform();
        builder.sendKeys(Keys.DELETE).perform();
        sleep();

    }
    // Clears the current control before it types in given input string
    public void clearType(String text)
    {
        log("Clearing selected field and typing '"+text+"'...");
        Actions builder = new Actions(driver);
        builder.sendKeys(Keys.CONTROL,"a").perform();
        sleep();
        builder.sendKeys(Keys.DELETE).perform();
        sleep();
        builder.sendKeys(text).perform();
        sleep();

    }
    public void select(String elementLocator, String selection)
    {
        if (selection == null || selection==""){return;}
        WebElement element = getElement(elementLocator);
        select(element,selection);

    }
    public void select(WebElement element, String selection)
    {
        waitForObjectVisible(element);
//        click(element);
        logInteraction(element, "SELECT '"+selection+"' from the dropdown.");
        Select selector = new Select(element);
        List<WebElement> options = selector.getOptions();
        for(WebElement option: options)
        {
            if(option.getText().contains(selection))
            {
                option.click();
                return;
            }
        }
        fail("Selection '"+selection+"'not found!!");
    }
    // Sets a checkbox value to the given boolean value (true=check, false=un-check)
    public void checkBox(String elementLocator, boolean check)
    {
        WebElement element = getElement(elementLocator);
        waitForObjectVisible(element);
        logInteraction(element, "CHECK box - set to '"+check+"'");
        boolean isSelected = element.isSelected();
        if (isSelected!=check)
        {
            element.click();
        }

    }
    // Scrolls to an element that is needed.
    public void moveToElement(String elementLocator)
    {
        WebElement webElement = getElement(elementLocator);
        Actions builder = new Actions(driver);
        builder.moveToElement(webElement).perform();
        sleep();
    }
    public void upload(String elementLocator, String filename)
    {
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        WebElement inputField = getElement(elementLocator);
        setElementVisible(inputField);
        executor.executeScript("arguments[0].setAttribute('value', '" + filename +"')", inputField);
        executor.executeScript("arguments[0].blur()", inputField);
    }
    public void navigate(String url)
    {
        log("NAVIGATING to the following URL:"+url);
        driver.navigate().to(url);
    }
    public void refresh()
    {
        log("RELOADING current page...");
        sleep();
        driver.navigate().refresh();
        sleep();
    }
    // Returns text attribute associated to the given locator expression
    public String getText(String elementLocator)
    {
        WebElement element = getElement(elementLocator);
        waitForObjectVisible(element);
        return element.getText();
    }
    public String getXpathText(String elementLocator)
    {
        return driver.findElement(By.xpath(elementLocator)).getText();
    }
    public void setAttribute(String elementLocator, String attribute, String value)
    {
        WebElement element = getElement(elementLocator);
        setAttribute(element, attribute, value);
    }
    public void setAttribute(WebElement element, String attribute, String value)
    {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);",
                element, attribute, value);

    }
    public void setElementVisible(String elementLocator)
    {
        setAttribute(elementLocator, "style", "display: block;");
    }
    public void setElementVisible(WebElement element)
    {
        setAttribute(element, "style", "display: block;");
    }
    // Determines element Locator method and returns WebElement
    public WebElement getElement(String elementLocator) {
        WebElement webElement;
        new WebDriverWait(driver, TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("html")));
        if (elementLocator.contains(">")) {
            webElement = driver.findElement(By.cssSelector("html"));
            List<String> elementLocators = splitSeparatedString(elementLocator);
            for (String loc : elementLocators) {
                if (loc.contains(".//*[") || loc.contains("//")) {
                    webElement = webElement.findElement(By.xpath(loc));
                } else {
                    webElement = webElement.findElement(By.className(loc));
                }
            }
            return webElement;
        }
        if (elementLocator.contains(".//*[") || elementLocator.contains("//")) {
            webElement = driver.findElement(By.xpath(elementLocator));

        }
        else {

            webElement = getJqueryElement(elementLocator);
        }
        return webElement;
    }
    // Determines element Locator method and returns WebElement
    public List<WebElement> getElements(String elementLocator) {
        List<WebElement> webElement=null;
        new WebDriverWait(driver, TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("html")));

        if (elementLocator.contains(".//*[") || elementLocator.contains("//")) {
            for(int second = 0; second<=TIMEOUT ; second++) {
                webElement = driver.findElements(By.xpath(elementLocator));
                if (webElement.size() > 0) {
                    break;
                }
            }
        }
        else {
            for(int second = 0; second<=TIMEOUT ; second++) {
                webElement = getJqueryElements(elementLocator);
                if (webElement.size()>0)
                {
                    break;
                }
            }
        }
        return webElement;
    }
    // Inject jQuery into the page
    protected void injectJqueryIntoPage()
    {
        injectJqueryIntoPage(driver);
    }
    // Injects jQuery script into the current page
    public void injectJqueryIntoPage(WebDriver driver)
    {
        try {
            URL jqueryUrl = Resources.getResource("jquery-3.1.1.js");
            String jqueryText = Resources.toString(jqueryUrl, Charsets.UTF_8);
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript(jqueryText);
        }
        catch(Exception e) {
            System.out.println("Unable to find jQuery resource!");

        }
    }
    private String readFile(String file) throws IOException {
        Charset cs = Charset.forName("UTF-8");
        FileInputStream stream = new FileInputStream(file);
        try {
            Reader reader = new BufferedReader(new InputStreamReader(stream, cs));
            StringBuilder builder = new StringBuilder();
            char[] buffer = new char[8192];
            int read;
            while ((read = reader.read(buffer, 0, buffer.length)) > 0) {
                builder.append(buffer, 0, read);
            }
            return builder.toString();
        }
        finally {
            stream.close();
        }
    }
    // Retrieves web element using jquery locator
    private WebElement getJqueryElement(String elementLocator)
    {
        WebElement element= null;
        JavascriptExecutor js= (JavascriptExecutor)driver;

        String findElement = "return $(\"" + elementLocator + "\").get(0);";
        for(int second = 0; ; second++)
        {
            if(second>TIMEOUT){
                fail("It took longer than " + TIMEOUT + " seconds in the attempt to wait for object '"+elementLocator+"'!");
            }
            try{

                element = (WebElement)js.executeScript(findElement);
            }
            catch (Exception e)
            {
//                if(e.getMessage().contains("not defined")) {
//                    Assert.assertNotNull("Error!! ...in the element selector string!\nCheck locator string: '" + elementLocator + "'",element);
//                }
//                else {
                // Either the page hasn't reloaded and/or the jQuery code was not injected successfully
                injectJqueryIntoPage();
//                }
            }
            if (element!=null)
            {
                return element;
            }
            sleep(millisecondsToSleep);
        }
    }
    // Retrieves web element using jquery locator
    public List<WebElement> getJqueryElements(String elementLocator)
    {
        List<WebElement> element= null;
        JavascriptExecutor js= (JavascriptExecutor)driver;

        String findElement = "return $(\"" + elementLocator + "\").get();";
        for(int second = 0; ; second++)
        {
            if(second>TIMEOUT){
                fail("It took longer than " + TIMEOUT + " seconds in the attempt to wait for object '"+elementLocator+"'!");
            }
            try{

                element = (List<WebElement>)js.executeScript(findElement);
            }
            catch (Exception e)
            {
                if(!e.getMessage().contains("not defined")) {
                    Assert.assertNotNull(element,"Error!! ...in the element selector string!\nCheck locator string: '"
                            + elementLocator + "'" );
                }
                else {
                    // Either the page hasn't reloaded and/or the jQuery code was not injected successfully
                    injectJqueryIntoPage();
                }
            }
            if (element!=null)
            {
                return element;
            }
            sleep(millisecondsToSleep);
        }
    }
    // Returns element associated to the given locator expression
    private WebElement getElementByLink(String elementLocator)
    {
        return driver.findElement(By.linkText(elementLocator));
    }
    // This methods waits for a object to be displayed before using it (in case it's still initializing)
    public void waitForObjectVisible(WebElement element)
    {

        for(int second = 0; ; second++)
        {
            if(second>TIMEOUT){
                fail("It took longer than " + TIMEOUT + " seconds in the attempt to wait for object '"+element.getText()+"'!");
            }
            if (element.isDisplayed())
            {
                return;
            }
            if(second>longerWait)
            {
                setElementVisible(element);
            }
            sleep(millisecondsToSleep);
        }
    }
    public void waitForObjectVisible(String elementLocator)
    {
        log("Waiting for Element "+elementLocator+" to show...");
        for(int second = 0; ; second++)
        {
            List<WebElement> elements = getElements(elementLocator);
            if(second>TIMEOUT){
                fail("It took longer than " + TIMEOUT + " seconds in the attempt to wait for object '"+elementLocator+"'!");
            }
            if(elements.size()<=0)
            {
                continue;
            }
            if (elements.get(0).isDisplayed())
            {
                log("Element "+elementLocator+" shows now as displayed!");
                return;
            }
            sleep();
        }
    }
    // This methods waits for a object to be displayed before using it (in case it's still initializing)
    public void waitForObjectEnabled(WebElement element)
    {

        for(int second = 0; ; second++)
        {
            if(second>TIMEOUT){
                fail("It took longer than " + TIMEOUT + " seconds in the attempt to wait for object '"+element.getText()+"'!");
            }
            if (element.isEnabled())
            {
                return;
            }
            sleep(millisecondsToSleep);
        }
    }
    // This methods waits for a object to be displayed before using it (in case it's still initializing)
    public void waitForObjectEnabled(String elementLocator)
    {

        WebElement element = getElement(elementLocator);
        waitForObjectEnabled(element);

    }
    public void waitForObjectVisibleEnabled(String elementLocator)
    {
        log("Wait for element:"+elementLocator+" becomes visible and enabled...");
        WebElement element = getElement(elementLocator);
        waitForObjectVisibleEnabled(element);

    }
    //    public void scrollToElement(String elementLocator)
//    {
//        log("Attempting to scroll to element: "+elementLocator);
//        WebElement element = getElement(elementLocator);
//        Actions actions = new Actions(driver);
//        actions.moveToElement(element);
//        actions.perform();
//    }
    public void waitForObjectVisibleEnabled(WebElement element)
    {
        waitForObjectVisible(element);
        waitForObjectEnabled(element);

    }
    // This methods puts the Thread to sleep for a given time
    public void sleep(int milliseconds)
    {
        try
        {
            if (milliseconds>1000) {
                log("Waiting for " + (milliseconds / 1000) + " second(s) before next interaction...");
            }
            Thread.sleep(milliseconds);
        }
        catch (Exception e)
        {
            System.out.println("Interrupt Exception Encountered.");
        }
    }
    public void sleep()
    {
        sleep(millisecondsToSleep);
    }
    // Verifies if value in element contains the comparison text
    public void verifyObjectContainsText(String elementLocator, String comparison)
    {
        WebElement element = getElement(elementLocator);
        verifyObjectContainsText(element, comparison);

    }
    // Verifies if value in element contains the comparison text
    public void verifyObjectContainsText(WebElement element, String comparison)
    {
        waitForObjectVisible(element);
        String actualText = element.getText();
        String elementInfo = getElementInfo(element);
        Assert.assertTrue(actualText.contains(comparison),"\nThe value in the given element '"
                +elementInfo
                +"' should be \n'"+comparison+"'\n, but the actual value is \n'"+actualText+"' !!"
        );
        log("VERIFICATION: Text '"+comparison+"' in element verified! (Element:'"+elementInfo+"')");

    }
    // Verifies if value in element contains the comparison text
    public void verifyPageSourceContainsText(String comparison)
    {
        String pageSource = driver.getPageSource();
        Assert.assertTrue(pageSource.contains(comparison),"\nThe page source should contain'"
                +comparison
                +"', but does not appear to be there !!"
        );
        log("VERIFICATION: Text '"+comparison+"' in element verified! (Element:'"+pageSource+"')");

    }
    public void verifyObjectNotContainText(String elementLocator, String comparison)
    {
        WebElement element = getElement(elementLocator);
        verifyObjectNotContainText(element, comparison);

    }
    public void verifyObjectNotContainText(WebElement element, String comparison)
    {
        waitForObjectVisible(element);
        String actualText = element.getText();
        String elementInfo = getElementInfo(element);
        Assert.assertFalse( actualText.contains(comparison),"\nThe text in the given element '"
                +elementInfo
                +"' should  not be \n'"+comparison+"'\n!!"
        );
        log("VERIFICATION: Text '"+comparison+"' does not exist in element as expected! (Element:'"+elementInfo+"')");

    }
    // Verifies if value in element contains the comparison value
    public void verifyCheckBoxValue(String elementLocator, boolean checked)
    {
        WebElement element = getElement(elementLocator);
        verifyCheckBoxValue(element, checked);

    }
    // Verifies if value in element contains the comparison value
    public void verifyCheckBoxValue(WebElement element, boolean checked)
    {
        boolean isSelected = element.isSelected();
        String elementInfo = getElementInfo(element);
        Assert.assertTrue(isSelected==checked,"\nThe selection value in the given element '"
                +elementInfo
                +"' should be \n'"+checked+"'\n, but the actual value is \n'"+isSelected+"' !!");
        log("VERIFICATION: Checkbox value of '"+checked+"' is verified! (Element:'"+elementInfo+"')");

    }
    // Verifies if value in element contains the comparison value
    public void verifyObjectContainsValue(String elementLocator, String comparison)
    {
        WebElement element = getElement(elementLocator);
        String actualText = element.getAttribute("value");
        if(actualText.contains("\u00A0")) //some browsers represent space characters different
        {
            actualText = actualText.replace("\u00A0", " ");
        }
        String elementInfo = getElementInfo(element);
        Assert.assertTrue(actualText.contains(comparison),"\nThe value in the given element '"
                +elementInfo
                +"' should be \n'"
                +comparison+"'\n, but the actual value is \n'"
                +actualText+"'\n !!"
        );
        log("VERIFICATION: Value '"+comparison+"' in the page element is as expected! (Element:'"+elementInfo+"')");

    }
    // Makes sure that the value of a control is empty (no value)
    public void verifyObjectValueEmpty(String elementLocator)
    {
        WebElement element = getElement(elementLocator);
        String actualText = element.getAttribute("value");
        Assert.assertTrue(actualText.isEmpty());
    }
    // Verifies if readonly value in element contains the comparison value
    public void verifyObjectContainsReadOnlyValue(String elementLocator, String comparison)
    {
        WebElement element = getElement(elementLocator);
        waitForObjectVisible(element);
        String actualText = element.getAttribute("readonly value");
        String elementInfo = getElementInfo(element);
        Assert.assertTrue( actualText.contains(comparison),"\nThe value in the given element '"
                +elementInfo
                +"' should be \n'"
                +comparison+"'\n, but the actual value is \n'"
                +actualText+"'\n !!"
        );
        log("VERIFICATION: Value '"+comparison+"' in element verified! (Element:'"+elementInfo+"')");

    }
    // Verifies if the attribute value in element contains the comparison value
    public void verifyObjectContainsAttribute(String attribute, String elementLocator, String valueComparison)
    {
        WebElement element = getElement(elementLocator);
        verifyObjectContainsAttribute(attribute, element, valueComparison);

    }
    // Verifies if the attribute value in element contains the comparison value
    public void verifyObjectContainsAttribute(String attribute, WebElement element, String valueComparison)
    {
        waitForObjectVisible(element);
        String actualText = element.getAttribute(attribute);
        String elementInfo = getElementInfo(element);
        Assert.assertTrue(actualText.contains(valueComparison),"\nThe value in the given element '"
                +elementInfo
                +"' should be \n'"
                +valueComparison+"'\n, but the actual value is \n'"
                +actualText+"'\n !!"
        );
        log("VERIFICATION: Value '"+valueComparison+"' in element verified! (Element:'"+elementInfo+"')");

    }
    // Verifies if the attribute value in element contains the comparison value
    public void verifyObjectDoesNotContainAttribute(String attribute, String elementLocator, String valueComparison)
    {
        WebElement element = getElement(elementLocator);
        verifyObjectDoesNotContainAttribute(attribute, element, valueComparison);

    }
    // Verifies if the attribute value in element contains the comparison value
    public void verifyObjectDoesNotContainAttribute(String attribute, WebElement element, String valueComparison)
    {
        waitForObjectVisible(element);
        String actualText = element.getAttribute(attribute);
        String elementInfo = getElementInfo(element);
        Assert.assertFalse(actualText.contains(valueComparison),"\nThe value in the given element '"
                +elementInfo
                +"' should not be \n'"
                +valueComparison+"'\n, the actual value is \n'"
                +actualText+"'\n !!"
        );
        log("VERIFICATION: Value '"+valueComparison+"' is not in element, as expected! (Element:'"+elementInfo+"')");

    }
    // Verifies if value in element contains the comparison value
    public void verifyObjectNotContainsValue(String elementLocator, String comparison)
    {
        WebElement element = getElement(elementLocator);
        waitForObjectVisible(element);
        String actualText = element.getAttribute("value");
        String elementInfo = getElementInfo(element);
        Assert.assertFalse(actualText.contains(comparison),"\nThe value in the given element '"
                +elementInfo
                +"' should not be \n'"
                +comparison+"'\n, but the actual value is \n'"
                +actualText+"'\n !!"
        );
        log("VERIFICATION: Value '"+comparison+"' does not show in element verified! (Element:'"+elementInfo+"')");

    }
    // Verifies that element exists in the DOM (but not if it is visible)
    public void verifyObjectExists(String elementLocator)
    {
        final byte TIMEOUT = 5;
        List<WebElement> elements = null;
        for (int x=0; x<TIMEOUT;x++)
        {
            elements = getElements(elementLocator);
            if (elements!=null && elements.size()>0)
            {
                break;
            }
            sleep();

        }
        Assert.assertFalse((elements==null || elements.size()==0),"\nThe The element '"
                +elementLocator
                +"' does not seem to exist here!!"
        );
        Assert.assertTrue(elements.get(0).isDisplayed(),"\nThe The element '"
                +elementLocator
                +"' does seem to exist, but does not show as visible!!"
        );


        log("VERIFICATION: element: '"+elementLocator+"' shows as expected.");
    }
    public void verifyObjectDisplayed(String elementLocator)
    {
        boolean displayed = isElementDisplayed(elementLocator);
        Assert.assertTrue(displayed,"Element should show as displayed, but does not!! "+elementLocator
        );
        log("VERIFICATION: element: '"+elementLocator+"' displays as expected.");
    }
    public void verifyObjectEnabled(String elementLocator)
    {
        boolean enabled = isElementEnabled(elementLocator);
        Assert.assertTrue(enabled,"Element should show as enabled, but does not!! "+elementLocator
        );
        log("VERIFICATION: element: '"+elementLocator+"' is Enabled as expected.");
    }
    // Verifies if a given element is disnabled
    public void verifyObjectDisabled(String elementLocator)
    {
        boolean enabled = isElementEnabled(elementLocator);
        Assert.assertFalse(enabled ,"Element should not show as enabled!! "+elementLocator);

        log("VERIFICATION: element: '"+elementLocator+"' is Disabled as expected.");

    }
    // Returns a boolean about the enabled state of the given element
    public boolean isElementEnabled(String elementLocator)
    {
        log("Checking if element: '"+elementLocator+"' is enabled...");
        boolean enabled=false;
        WebElement element = getElement(elementLocator);
        waitForObjectVisible(element);
        if (element.getAttribute("disabled")!=null)
        {
            return false;
        }
        else if (element.isEnabled())
        {
            enabled = true;
        }
        return enabled;
    }
    // Returns a boolean if a element exists currently on the site
    public boolean doesElementExist(String elementLocator) {
        log("Checking if element: '" + elementLocator + "' does exist...");
        final byte TIMEOUT = 2;
        List<WebElement> elements = null;
        for (int x = 0; x < TIMEOUT; x++) {
            elements = getElements(elementLocator);

            if (elements != null && elements.size() > 0) {
                System.out.println(elements);
                return true;
            }
            sleep();

        }
        System.out.println(elements);
        return false;

    }
    // Verifies that element is displayed/visible
    public boolean isElementDisplayed(String elementLocator)
    {
        log("Checking if element: '"+elementLocator+"' is displayed...");
        if(doesElementExist(elementLocator))
        {
            log("Element shows as displayed!");
            return getElement(elementLocator).isDisplayed();
        }
        return false;

    }
    // Verifies that element does not exist on active page
    public void verifyObjectNotExist(String elementLocator)
    {
        // For an object that does not exist, we first have to wait for a potential load finish of the site
        List<WebElement> elements = getElements(elementLocator);
        if(elements.size()>0) {
            Assert.assertFalse(elements.get(0).isDisplayed(),"\nThe The element '" + elementLocator + "' should not be showing here!!" ) ;
        }

        log("VERIFICATION: element: '"+elementLocator+"' does not unexpectendly show.");

    }
    public void verifyObjectSelection(String elementLocator, String selection){
        log("Checking if dropdown: '"+elementLocator+"' shows '"+selection+"' as selected...");
        WebElement element = getElement(elementLocator);
        waitForObjectVisible(element);
        String elementInfo = getElementInfo(element);
        Select selector = new Select(element);
        String selectedValue = selector.getFirstSelectedOption().getText();
        Assert.assertTrue(selectedValue.contains(selection),"\nThe value in the given element '"
                +elementInfo
                +"' should be \n'"
                +selection+"'\n, but the actual value is \n'"
                +selectedValue+"'\n !!"
        );
        log("VERIFICATION: Selection '"+selection+"' in element verified! (Element:'"+elementInfo+"')");


    }
    // Returns the value that is currently selected in the selection control
    public String getCurrentSelection(String elementLocator)
    {
        WebElement element = getElement(elementLocator);
        return getCurrentSelection(element);
    }
    public String getCurrentSelection(WebElement element)
    {
        waitForObjectVisible(element);
        String elementInfo = getElementInfo(element);
        Select selector = new Select(element);
        String selectedValue = selector.getFirstSelectedOption().getText();
        log("\nThe value in the given element '"
                +elementInfo
                +"' is: ''"+selectedValue+"'");
        return selectedValue;

    }
    // Verifies if value in element contains the comparison value
    public void verifyObjectHasAttribute(String elementLocator, String attribute, String comparison)
    {
        WebElement element = getElement(elementLocator);
        waitForObjectVisible(element);
        String elementInfo = getElementInfo(element);
        Assert.assertTrue(element.getAttribute(attribute).contains(comparison),"\nValue:\n'"
                +comparison+"' not found in the attribute:\n'"+attribute+"'\n in Element:\n"+
                elementInfo+"\n!!");
        log("VERIFICATION: Value '"+comparison+"' in element verified! (Element:'"+elementInfo+"')");

    }
    //Splits a > separated string and creates a list out of it.
    public List<String> splitSeparatedString(String elementLocator)
    {
        return splitSeparatedString(elementLocator, ">");
    }
    //Splits a > separated string and creates a list out of it.
    public List<String> splitSeparatedString(String source, String separator)
    {
        List<String> items = Arrays.asList(source.split("\\s*"+separator+"\\s*"));
        return items;
    }
    // Logging
    public void log(String string)
    {
        String time = AutomationHelper.getFormatedDate(AutomationHelper.getISOdate()+" "+AutomationHelper.getISOtime());
        System.out.println("** "+time+" "+string);
    }
    // Logging element interactions (click, type, select...)
    private void logInteraction(WebElement element, String message)
    {
        String elementInfo = getElementInfo(element);

        log("Action: "+message+ ", on element: '"+elementInfo+"'");

    }
    // Retrieving information
    protected static String getElementInfo(WebElement element)
    {
        String elementInfo;
        String elementID = "";
        String elementName = "";
        String elementTagName = "";
        String elementValue = "";
        String elementText = "";
        String elementClass = "";
        try {
            // accessing attribute by likeliness of success
            elementTagName = element.getTagName();
            elementID = element.getAttribute("id");
            elementName = element.getAttribute("name");
            elementText = element.getText();
            if (elementText.length()>10) {
                elementText = elementText.substring(0,10)+"...";
            }
            elementValue = element.getAttribute("value");
            elementClass = element.getAttribute("class");

        }
        catch (Exception e)
        {
            // one  or more element attributes could not be retrieved.
        }
        finally {
            elementInfo = "<"+elementTagName+">"+elementID+"~"+elementText+"~"+elementName+"~"+elementValue;
            if (elementInfo.length()<30 && elementClass!="null")
            {
                elementInfo = elementInfo + "~"+elementClass;
            }
            return elementInfo;
        }

    }
    public  String buildReactElementId(String elementName, String elementCode) {
        StringBuilder sB = new StringBuilder();
        sB.append(elementName);
        sB.append(elementCode);
        String builtReactElementId = sB.toString();
        return builtReactElementId;
    }
    // Weits for given element to visually disappear (or element is non-existent, whatever comes first)
    public void waitForElementNotVisible(String elementLocator) {
        log("Checking if element: '" + elementLocator + "' does not exist...");
        List<WebElement> elements = null;
        byte time = 0;
        try {
            for (byte x = 0; x <= TIMEOUT; x++) {
                elements = getElements(elementLocator);
                time = x;
                if (elements == null) {
                    log("Element does not exists, so is also not visible!");
                    break;
                } else if (elements.size() > 0 && elements.get(0).isDisplayed()) {
                    sleep(1000);
                } else {
                    log("Element '" + elementLocator + "' disappeared after " + x + " seconds!");
                    break;
                }
            }
        }
        // Dealing with potential stale element exception
        catch (StaleElementReferenceException e)
        {
            log("Element appears to have become stale. Assume it disappeared...");
        }
        catch(Exception e)
        {
            fail("Unknown error while waiting for element to disappear!! ");
        }
        // Dealing with timeout
        if (time == TIMEOUT) {
            fail("Element '"+ elementLocator +"' did not disappear within 60 seconds!!");
        }
    }

    public void clickAt(String elementLocator, int x, int y) {
        WebElement element = getElement(elementLocator);
        Actions actions = new Actions(driver);
        actions.moveToElement(element, x, y).click().build().perform();
        logInteraction(element, "CLICK");
    }
    // Waiting until a loader icon (spinner etc.) appears and disappears
    public void waitForLoader(String elementLocator) {
        log("Waiting for given load spinner to finish...");
        sleep();
        waitForElementNotVisible(elementLocator);

    }
}
