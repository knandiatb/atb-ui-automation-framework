/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package utils.exceptions;

/**
 * Exception class
 *
 * @author Koyel Nandi
 */
public class AutomationException extends Exception {

    private static final long serialVersionUID = 1L;

    public AutomationException(String message) {
        super(message);
    }

    public AutomationException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public AutomationException(Throwable cause) {
        super(cause);
    }

}
