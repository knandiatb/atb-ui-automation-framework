/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package utils.exceptions;

import org.openqa.selenium.WebDriverException;

/**
 * Exception class. This exception will be thrown when webdriver creation is not successful.
 *
 * @author Koyel Nandi
 */
public class UnableToCreateWebDriverException extends WebDriverException {

    private static final long serialVersionUID = 1L;

    public UnableToCreateWebDriverException(Throwable cause) {
        super(cause);
    }

    public UnableToCreateWebDriverException(String message) {
        super(message);
    }

    public UnableToCreateWebDriverException(String message, Throwable cause) {
        super(message, cause);
    }
}
