/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package utils.exceptions;

/**
 *
 * @author Koyel Nandi
 */
public class UnexpectedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UnexpectedException(Throwable cause) {
        super(cause);
    }

    public UnexpectedException(String message) {
        super(message);
    }

    public UnexpectedException(String message, Throwable cause) {
        super(message, cause);
    }
}