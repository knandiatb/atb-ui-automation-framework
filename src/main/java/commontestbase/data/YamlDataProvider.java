/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package commontestbase.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.lang3.StringUtils;
import org.yaml.snakeyaml.Yaml;


import com.google.common.base.CaseFormat;

/**
 * A class to populate the Bean from Yaml
 *
 * @author Koyel Nandi
 */
public class YamlDataProvider implements IDataProvider {


    public Map<String, Object> getCustomData(String id, String yamlFilePath) throws IOException {
        try (InputStream inStream = new FileInputStream(new File(yamlFilePath))) {
            Iterable<Object> allObjects = new Yaml().loadAll(inStream);
            return loadMap(allObjects, id);
        }
    }

    @Override
    public <T> Map<String, Object> getData(Class<T> clazz, String id) throws IOException {
        try (InputStream inStream = getInputStream(clazz)) {
            Iterable<Object> allObjects = new Yaml().loadAll(inStream);
            return loadMap(allObjects, id);
        }
    }

    public <T> InputStream getInputStream(Class<T> clazz) throws IOException {
        String className = getRelativePath(clazz);
        TestEnv.ENV currentTestingLocale = TestEnv.getCurrentTestLocal();
        String yamlFilePath = "testdata/" + currentTestingLocale.getName() + "/" + className + ".yaml";
        InputStream inStream = ClassLoader.getSystemResourceAsStream(yamlFilePath);
        if (inStream == null) {
            throw new FileNotFoundException("File does not exist: " + yamlFilePath);
        }
        return inStream;
    }

    public <T> T getDataBean(Class<T> clazz, String id) throws Exception {
        Map<String, Object> data = getData(clazz, id);
        try {
            T bean = clazz.newInstance();
            return populateBean(data, clazz, bean);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | ClassNotFoundException
                | NoSuchMethodException | SecurityException e) {
            System.out.println("Error thrown from getDataBean");
        }
        return null;
    }

    public <T> T getDataBeanFromCustomPath(Class<T> clazz, String id, String yamlFilePath) throws Exception {
        Map<String, Object> data = getCustomData(id, yamlFilePath);
        try {
            T bean = clazz.newInstance();
            return populateBean(data, clazz, bean);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | ClassNotFoundException
                | NoSuchMethodException | SecurityException e) {
            System.out.println("Error thrown from getDataBeanFromCustomPath");
        }
        return null;
    }

    public <T> T getDataBeanWithOverride(Map<?, ?> beanDataMap, Class<?> clazz, T bean) throws Exception {
        try {
            return populateBean(beanDataMap, clazz, bean);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | ClassNotFoundException
                | NoSuchMethodException | SecurityException e) {
            System.out.println("Error thrown from getDataBeanWithOverride");
        }
        return null;
    }

    @Override
    public <T> List<T> getDataList(Class<T> clazz) throws Exception {
        Map<String, Object> allObjects = getData(clazz, null);
        List<T> listBean = new ArrayList<>();
        for (Entry<String, Object> entry : allObjects.entrySet()) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put(entry.getKey(), entry.getValue());
            try {
                listBean.add(populateBean(map, clazz, clazz.newInstance()));
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException
                    | ClassNotFoundException | NoSuchMethodException | SecurityException e) {
                System.out.println("Error thrown from getDataList");
            }
        }
        return listBean;
    }

    private String getRelativePath(@SuppressWarnings("rawtypes") Class clazz) {
        String className = clazz.getSimpleName();
        className = StringUtils.removeIgnoreCase(className, "bean");
        className = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_HYPHEN, className);
        return className;
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> loadMap(Iterable<Object> allObjects, String id) {
        while (allObjects.iterator().hasNext()) {
            Object object = allObjects.iterator().next();
            Map<String, Object> map = (Map<String, Object>) object;
            setRandomName(map);
            if (null == id)
                return map;
            if (map.get("dataset").equals(id)) {
                return map;
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private <T> T populateBean(Map<?, ?> beanDataMap, Class<?> clazz, T bean) throws Exception {
        Map<?, ?> processedBeanMap = beanDataMap.entrySet().stream().collect(Collectors.toMap(Entry::getKey,
                e -> e.getValue() instanceof Date ? ((Date) e.getValue()).toInstant() : e.getValue()));
        BeanUtils.populate(bean, (Map<String, ? extends Object>) processedBeanMap);
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            String type = field.getType().getName();
            if (field.getType().isPrimitive() || field.getType().toString().contains("String")) {
                continue;
            }
            if (type.endsWith("Bean")) {
                String fieldName = field.getName();
                String fieldKey = fieldName + "_id";
                if (!processedBeanMap.containsKey(fieldKey))
                    continue;
                String fieldValue = processedBeanMap.get(fieldKey).toString();
                if (fieldValue.isEmpty())
                    continue;
                T subBean = (T) getDataBean(Class.forName(type), fieldValue);
                BeanUtilsBean.getInstance().setProperty(bean, fieldName, subBean);
            }
            if (type.contains("java.util.List")) {
                ParameterizedType listType = (ParameterizedType) field.getGenericType();
                Class<?> listClass = (Class<?>) listType.getActualTypeArguments()[0];
                List<T> listBean = new ArrayList<>();
                String fieldName = field.getName();
                String fieldKey = fieldName + "_list";
                if (!processedBeanMap.containsKey(fieldKey))
                    continue;
                ArrayList<String> fieldValue = (ArrayList<String>) processedBeanMap.get(fieldKey);
                for (String element : fieldValue) {
                    if (element.isEmpty())
                        continue;
                    T subBean = (T) getDataBean(Class.forName(listClass.getCanonicalName()), element);
                    listBean.add(subBean);
                }
                BeanUtilsBean.getInstance().setProperty(bean, fieldName, listBean);
            } else {
                String fieldKey = field.getName() + "_enum";
                if (!processedBeanMap.containsKey(fieldKey))
                    continue;
                String fieldValue = processedBeanMap.get(fieldKey).toString();
                if (fieldValue.isEmpty())
                    continue;
                @SuppressWarnings("rawtypes")
                Class cls = Class.forName(type);
                Object o = Enum.valueOf(cls, fieldValue);
                BeanUtilsBean.getInstance().setProperty(bean, field.getName(), o);
            }
        }
        return bean;
    }

    private void setRandomName(Map<String, Object> data) {
        for (Entry<String, Object> item : data.entrySet()) {
            if (item.getValue() != null) {
                String value = item.getValue().toString();
                if (value.contains("[R]")) {
                    String newValue = value.replace("[R]", String.valueOf(System.currentTimeMillis()));
                    item.setValue(newValue);
                }
            }
        }
    }

}
