/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package commontestbase.data;

/**
 * An interface to handle Data.
 *
 * @author Koyel Nandi
 */
public interface IDataBean {

    public String getValue();

    public <T> Class<T> getClazz();

}
