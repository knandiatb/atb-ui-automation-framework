/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package commontestbase.data;


import com.google.common.primitives.Ints;
import configmanager.ConfigManager;
import one.util.streamex.StreamEx;
import org.testng.annotations.DataProvider;
import org.yaml.snakeyaml.Yaml;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * A class to get the DataProvider from .yaml
 *
 * @author Koyel Nandi
 */
public class Yaml2DataProvider {

    private static Integer maxDataSets = Ints
            .tryParse(ConfigManager.getConfigManager().getConfig("maxDataSets", "unlimited"));

    private Yaml2DataProvider() {
    }

    @DataProvider(name = "genericDataProvider", parallel = true)
    public static Object[][] getData(Method testMethod) throws Exception {
        String yamlFileName = getScenarioFileName(testMethod);
        TestEnv.ENV currentTestingLocale = TestEnv.getCurrentTestLocal();
        String yamlFilePath = "testdata/" + currentTestingLocale.getName() + "/dataprovider/" + yamlFileName + ".yaml";
        try (InputStream inStream = ClassLoader.getSystemResourceAsStream(yamlFilePath)) {
            if (inStream == null) {
                throw new FileNotFoundException("File does not exist: " + yamlFilePath);
            }
            Iterable<Object> allObjects = new Yaml().loadAll(inStream);
            if (allObjects.iterator().hasNext()) {
                Object object = allObjects.iterator().next();
                return convertToObjectArray((LinkedHashMap<?, ?>) object);
            }
            return null;
        }
    }

    private static Object[][] convertToObjectArray(LinkedHashMap<?, ?> linkedHashMap) {
        return StreamEx.of((linkedHashMap).entrySet()).map(Map.Entry::getValue).map(value -> new Object[] { value })
                .chain(stream -> maxDataSets != null ? stream.limit(maxDataSets) : stream).toArray(Object[][]::new);
    }

    public static String getScenarioFileName(Method testMethod) throws Exception {
        if (testMethod == null)
            throw new IllegalArgumentException("Test Method context cannot be null.");
        Scenario name = testMethod.getAnnotation(Scenario.class);
        if (name == null)
            throw new IllegalArgumentException("Test Method context has no DataProviderArguments annotation.");
        if (name.value().length() == 0)
            throw new IllegalArgumentException("Test Method context has a malformed DataProviderArguments annotation.");
        return name.value();
    }

    @SuppressWarnings("unchecked")
    @DataProvider(name = "multiYamlDataProvider", parallel = true)
    public static Object[][] fileDataProvider(Method testMethod) throws Exception {
        String yamlFileName = getScenarioFileName(testMethod);
        TestEnv.ENV currentTestingLocale = TestEnv.getCurrentTestLocal();
        String yamlFilePath = "testdata/" + currentTestingLocale.getName() + "/dataprovider/" + yamlFileName + ".yaml";
        InputStream inStream = ClassLoader.getSystemResourceAsStream(yamlFilePath);
        Iterable<Object> allObjects = new Yaml().loadAll(inStream);
        Object object = allObjects.iterator().next();
        List<Object> listOfYamlObject = new ArrayList<Object>();
        while (object != null) {
            Map<String, Object> map = (Map<String, Object>) object;
            String fileName = map.values().toString().replaceAll("\\[([^.]+).*", "$1");
            listOfYamlObject.add(getNestedYamlObject(fileName));
            object = allObjects.iterator().next();
        }
        return convertListOfObjectToObjectArray(listOfYamlObject);
    }

    public static Object getNestedYamlObject(String yamlFileName) {
        TestEnv.ENV currentTestingLocale = TestEnv.getCurrentTestLocal();
        String yamlFilePath = "testdata/" + currentTestingLocale.getName() + "/dataprovider/" + yamlFileName + ".yaml";
        InputStream inStream = ClassLoader.getSystemResourceAsStream(yamlFilePath);
        Iterable<Object> allObjects = new Yaml().loadAll(inStream);
        if (allObjects.iterator().hasNext()) {
            return allObjects.iterator().next();
        }
        return null;
    }

    private static Object[][] convertListOfObjectToObjectArray(List<Object> listOfObject) {
        return StreamEx.of(listOfObject).flatMap(object -> ((LinkedHashMap<?, ?>) object).entrySet().stream())
                .map(Map.Entry::getValue).map(value -> new Object[] { value })
                .chain(stream -> maxDataSets != null ? stream.limit(maxDataSets) : stream).toArray(Object[][]::new);
    }
}
