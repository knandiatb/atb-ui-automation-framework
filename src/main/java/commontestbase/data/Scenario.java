/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package commontestbase.data;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * Annotation for feeding arguments to methods conforming to the "@DataProvider" annotation type.
 *
 * @author Koyel Nandi
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Scenario {

    String value();
}
