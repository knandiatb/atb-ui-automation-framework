/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package commontestbase.data;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;


import org.apache.commons.beanutils.BeanUtilsBean;

import utils.exceptions.UnexpectedException;

/**
 * A Factory Class to populate Java Bean object.
 *
 * @author Koyel Nandi
 */

public class DataBeanFactory {

    private DataBeanFactory() {
    }

    public static <T> T loadDataBean(IDataBean iDataBean) {
        String id = iDataBean.getValue();
        Class<?> clazz = iDataBean.getClazz();
        YamlDataProvider iDataProvider = new YamlDataProvider();
        try {
            return (T) iDataProvider.getDataBean(clazz, id);
        } catch (Exception e) {
            throw new UnexpectedException(e);
        }
    }


    public static <T> T loadDataBeanWithOverride(IDataBean iDataBean, Map<String, ?> dataSet) throws Exception {
        String id = iDataBean.getValue();
        Class<?> clazz = iDataBean.getClazz();
        YamlDataProvider iDataProvider = new YamlDataProvider();
        T bean = (T) iDataProvider.getDataBean(clazz, id);
        return iDataProvider.getDataBeanWithOverride(dataSet, clazz, bean);
    }

	public static <T> T loadDefaultDataBeanWithOverride(IDataBean overrideDataBean) {
		Class<?> clazz = overrideDataBean.getClazz();
		YamlDataProvider iDataProvider = new YamlDataProvider();
		try {
			T bean1 = (T) iDataProvider.getDataBean(clazz, "default");
			T bean2 = (T) iDataProvider.getDataBean(clazz, overrideDataBean.getValue());
			return (T) IgnoreNullAndCopy(bean1, bean2);
		} catch (Exception e) {
			throw new UnexpectedException(e);
		}
	}

	public static Object IgnoreNullAndCopy(Object dest, Object source)
			throws IllegalAccessException, InvocationTargetException {
		new BeanUtilsBean() {
			@Override
			public void copyProperty(Object dest, String name, Object value)
					throws IllegalAccessException, InvocationTargetException {
				if (value != null) {
					super.copyProperty(dest, name, value);
				}
			}
		}.copyProperties(dest, source);
		return dest;
	}
}
