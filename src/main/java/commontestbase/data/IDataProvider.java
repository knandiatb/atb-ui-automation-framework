/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package commontestbase.data;

import java.util.List;
import java.util.Map;

/**
 * An interface to handle DataProvider.
 *
 * @author Koyel Nandi
 */
public interface IDataProvider {

    <T> List<T> getDataList(Class<T> clazz) throws Exception;

    public <T> Map<String, Object> getData(Class<T> clazz, String id) throws Exception;

}
