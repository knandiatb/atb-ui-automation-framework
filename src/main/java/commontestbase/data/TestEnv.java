/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package commontestbase.data;


import configmanager.ConfigManager;


/**
 * A Class to control different types of Test Environment
 *
 * @author Koyel Nandi
 */
public class TestEnv {

    private static ENV currentTestEnv = ENV.DEFAULT;
    private static ConfigManager cm;

    static {
        cm = ConfigManager.getConfigManager();
        try {
            setTestEnv(cm.getConfig("app.TestEnv"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set the test locale
     * 
     * @param testEnvName
     * @throws Exception
     */
    public static void setTestEnv(String testEnvName) throws Exception {
        if (null == testEnvName)
            return;
        for (ENV env : ENV.values()) {
            if (env.getName().equals(testEnvName)) {
                currentTestEnv = env;
                return;
            }
        }
        throw new Exception("The given TestEnv " + testEnvName + " is not supported.");
    }

    public static ENV getCurrentTestLocal() {
        return currentTestEnv;
    }

    public enum ENV {

        DEFAULT("env_QA"),
        ENV_UAT("env_UAT"),
        ENV_PROD("env_PROD");

        private String name;

        ENV(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }
    }
}
