/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package commontestbase;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * The annotated method will run immediately before each test method. If context, parallel is set as method,
 * this will instantiate the driver before doing the task mentioned in this scope.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface PreMethod {

}
