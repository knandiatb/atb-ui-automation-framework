/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */

package commontestbase;

import configmanager.ConfigManager;
import org.openqa.selenium.WebDriver;
import org.testng.*;
import org.testng.annotations.*;
import utils.exceptions.AutomationException;
import utils.exceptions.UnableToCreateWebDriverException;
import webdriverbuilder.WebDriverFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

/**
 * The base class of all Tests implementation. This class should be extended by all test classes.
 *
 * @author Koyel Nandi
 *
 */

public abstract class ATBTestBase {

    private static volatile ConfigManager cm;
    private static String testConfigPath = "/config";
    private String browserValue;
    private String parallelBy;

    /**
     * To store a created ConfigiManager object so that it can be accessible safely in multi-thread environment.
     */
    protected static final ThreadLocal<ConfigManager> cmThreadLocal = new ThreadLocal<ConfigManager>();

    public ATBTestBase() {
    }

    @BeforeMethod(alwaysRun = true)
    @Parameters(value={"environment"})
    public void setupBeforeMethodTest (@org.testng.annotations.Optional String environment, ITestContext iTestContext) {
        setupEnvironment(environment);
        if(parallelBy.equals("method")){
            System.out.println("Base BeforeMethod started....");
            WebDriverFactory.setDriver(browserValue);
        }
        invokeAnnotatedMethod(Reporter.getCurrentTestResult(), PreMethod.class);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDownAfterMethod() {
        invokeAnnotatedMethod(Reporter.getCurrentTestResult(), PostMethod.class);
        if(parallelBy.equals("method")){
            System.out.println("Base AfterMethod started....");
            WebDriver driver = WebDriverFactory.getDriver();
            if (driver != null) {
                System.out.println("Base AfterMethod started to quit the driver....");
                driver.quit();
            }
        }
    }

    @BeforeClass(alwaysRun = true)
    @Parameters(value={"environment"})
    public void setupBeforeClassTest (@org.testng.annotations.Optional String environment, ITestContext iTestContext) {
        setupEnvironment(environment);
        if(parallelBy.equals("class")){
            System.out.println("Base Class's BeforeClass started....");
            WebDriverFactory.setDriver(browserValue);
        }
        invokeAnnotatedMethod(Reporter.getCurrentTestResult(), PreClass.class);
    }

    @AfterClass(alwaysRun = true)
    public void tearDownAfterClass() {
        invokeAnnotatedMethod(Reporter.getCurrentTestResult(), PostClass.class);
        if(parallelBy.equals("class")){
            WebDriver driver = WebDriverFactory.getDriver();
            if (driver != null) {
                System.out.println("Base Class's AfterClass started to quit the driver....");
                driver.quit();
            }
        }
    }


    public void setupEnvironment(@org.testng.annotations.Optional String environment){
        if (cm == null) {
            if(environment == null || environment.equals("qa")){
                testConfigPath = testConfigPath + "/qa.properties";
            }
            else if(environment.equals("dev")){
                testConfigPath = testConfigPath + "/dev.properties";
            }
            cm = getConfigManager();
        }
        parallelBy = getConfig("run.parallel");
        browserValue = getConfig("browser");
        if(parallelBy == null | browserValue == null){
            throw new UnableToCreateWebDriverException("run.parallel or browser is null");
        }

    }


    protected final WebDriver getDriver() {
        return WebDriverFactory.getDriver();
    }


    /**
     * Invoke the test class method marked with the specified annotation if it exists.
     *
     * @param result
     * @param annotation
     */
    private void invokeAnnotatedMethod(ITestResult result, Class<? extends Annotation> annotation) {
        Optional<Method> optionalMethod = Arrays.stream(result.getTestClass().getRealClass().getDeclaredMethods())
                .filter(m -> m.isAnnotationPresent(annotation)).findFirst();

        if (optionalMethod.isPresent()) {
            try {
                Method method = optionalMethod.get();
                System.out.println("Starting @" + annotation.getSimpleName() + " method...");
                method.invoke(result.getInstance(),
                        method.getParameterCount() == result.getParameters().length ? result.getParameters()
                                : new Object[0]);
                System.out.println("@" + annotation.getSimpleName() + " method complete.");
            } catch (Exception e) {
                Throwable cause = e.getCause();
                System.out.println("@" + annotation.getSimpleName() + " method invocation failed");
                if (result.getThrowable() == null) {
                    result.setThrowable(e);
                }
                result.setStatus(ITestResult.FAILURE);
            }
        }
    }


    /**
     * It returns a test configuration manager(ConfigManager) where you can get/set configurations in many different
     * ways. ConfigManager is a very flexible configuration manager enables you to pass key=value configuration element
     * from Java system properties, environment variables and/or a given property file.
     *
     * @return A test configuration manager where you can get/set configurations in many different ways.
     */
    protected final ConfigManager getConfigManager() {
        ConfigManager m = cmThreadLocal.get();
        if (m == null) {
            m = ConfigManager.createConfigManager(testConfigPath);
        }
        return m;
    }



    /**
     * A sugar code that can return a configuration value directly from this base class.
     *
     * @param key
     *            A configuration key
     * @return A configuration value
     */
    protected String getConfig(String key) {
        return getConfigManager().getConfig(key);
    }

    /**
     * A sugar code that can return a configuration value directly from this base class.
     *
     * @param key
     *            A configuration key
     * @param defaultValue
     * @return A configuration value
     */
    protected String getConfig(String key, String defaultValue) {
        return getConfigManager().getConfig(key, defaultValue);
    }

    /**
     * A sugar code that can return a configuration value directly from this base class.
     *
     * @param key
     * @return A configuration value as int.
     */
    protected int getConfigAsInt(String key) {
        return getConfigManager().getConfigAsInt(key);
    }

}
