/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */

package commontestbase;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * The annotated method will run immediately before test class terminating the WebDriver.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface PostClass {

}
