/**
 * Copyright 2018, ATB Financial.  All rights reserved.
 */
package commontestbase;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import utils.commonutils.AutomationHelper;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;


/**
 * The Listener class of ITestListener implementation. This class's methods will be called
 * for any particular state of test methods based on the requirements.
 *
 * @author Koyel Nandi
 * @author Norbert Griess
 */
public class TestNGBaseListener implements ITestListener {

    private static String filePath =System.getProperty("user.dir")+"/Test Reports/Screenshots/";

    @Override
    public void onStart(ITestContext arg0) {

        System.out.println("About to begin executing Suite " + arg0.getName());

    }
    @Override // What to do when test fails
    public void onTestFailure(ITestResult tr) {
        String  testName = tr.getMethod().getMethodName();
        System.out.println("\nTest "+ testName+" failed!!\n");
        try
        {
            Class clazz = tr.getTestClass().getRealClass();
            Field field = clazz.getField("driver");

            field.setAccessible(true);

            WebDriver driver = (WebDriver) field.get(tr.getInstance());
            takeScreenShot(testName, driver);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public void onTestStart(ITestResult res)
    {

    }
    public void onTestSkipped(ITestResult arg0) {

        System.out.println("Test was skipped");

    }
    public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {

    }
    @Override
    public void onTestSuccess(ITestResult tr)
    {
        String  testName = tr.getMethod().getMethodName();
        System.out.println("\nTest "+ testName+" Succeeded!!\n");
    }
    @Override
    public void onFinish(ITestContext context)
    {

//        System.out.println("\nClosing the browser...");
//        try {
//            driver.quit();
//        }
//        catch(Exception e)
//        {
//            System.out.print("It appears the driver wasn't initialized...");
//        }

    }

    // Takes screenshot and saves it in the project directory .../Test Reports/Screenshots/
    // With date/timestamp and method name in the filename
    public void takeScreenShot(String methodName, WebDriver driver) throws Exception {
        String filePath = System.getProperty("user.dir") + "/Test Reports/Screenshots/";
        String fileName = AutomationHelper.getISOdate() + "_" + AutomationHelper.getISOtime().replace(":", ".") +
                "_" + methodName + ".jpg";
        System.out.println("****** Screenshot filepath = ");
        System.out.println("" + filePath + fileName + "");
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs((OutputType.FILE));
        try {

            FileUtils.copyFile(scrFile, new File(filePath + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
